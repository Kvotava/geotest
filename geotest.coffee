@Places = new Mongo.Collection 'Places'
if Meteor.isClient
  
  Meteor.subscribe('Places', {lat: 47.5105794, lng: -122.19099059999999},1000)
  Template.closePlaces.helpers
    closePlaceData: () -> Places.find().fetch()


if Meteor.isServer 
  Meteor.startup () ->
    if Places.find().count() is 0
      console.log "INSERting 8888"
      Places.insert
        type: "beauty"
        name: "Salona Del Rey"
        loc: {
          type: "Point"
          coordinates: [-122.2222,47.4700]
        }
      console.log "INSERTED "
  Places._ensureIndex({'loc':'2dsphere'})
  Meteor.publish 'Places', (latLng, distance) ->
    statement = {loc: {$near: { $geometry: { type: "Point", coordinates: [ latLng.lng , latLng.lat ]},$maxDistance: distance }}}
    #statement = {}
    console.log statement
    Places.find(statement)
